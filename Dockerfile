##versión de php
FROM php:7.3-apache 
##instalación de editor
RUN apt-get update  
RUN apt-get install nano
#configuración de zona horaria
ENV TZ=America/Santiago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
## configuración de servidor apache
RUN a2enmod rewrite
COPY rest.conf /etc/apache2/sites-enabled/000-default.conf
##configuración e instalación de extensiones para el proyecto

##configuración de conexión con mysql (extesiones)
RUN  docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \ 
    && docker-php-ext-configure mysqli --with-mysqli=mysqlnd \
    ##instalación de extensiones de mysql
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli \
    ##instalación de extension para trabajar con sockets
    && docker-php-ext-install sockets