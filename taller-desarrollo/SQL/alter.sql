#para insertar los datos se optó por dejar el campo como nulo
ALTER TABLE SAR_USUARIO
ADD USU_RUT VARCHAR(25) ;

#actualizamos el rut de una secretaria con fines de generar datos dentro de el sistema
UPDATE SAR_USUARIO
SET USU_RUT="19909236-7"
WHERE USU_ID= 1

#actualizamos el rut de un admistrador con fines de generar datos dentro de el sistema
UPDATE SAR_USUARIO
SET USU_RUT="17000309-8"
WHERE USU_ID= 27

