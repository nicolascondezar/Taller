/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     23-06-2021 1:38:34                           */
/*==============================================================*/


drop table if exists SAR_EQUIPO;

drop table if exists SAR_LISTA_NEGRA;

drop table if exists SAR_PRESTAMO;

drop table if exists SAR_TIPO_EQUIPO;

drop table if exists SAR_TIPO_USUARIO;

drop table if exists SAR_USUARIO;

/*==============================================================*/
/* Table: SAR_EQUIPO                                            */
/*==============================================================*/
create table SAR_EQUIPO
(
   EQU_ID               integer not null auto_increment,
   TEQ_ID               integer,
   EQU_FECHA_INGRESO    date not null,
   EQU_FECHA_DESUSO     date,
   EQU_ESTADO           enum('a', 'i', 'r') not null,  
   primary key (EQU_ID)
);

/*==============================================================*/
/* Table: SAR_LISTA_NEGRA                                       */
/*==============================================================*/
create table SAR_LISTA_NEGRA
(
   LNG_ID               integer not null auto_increment,
   PRE_ID               integer not null,
   LNG_DETALLE          varchar(500) not null,
   LNG_IMAGEN           varchar(250) not null,
   primary key (LNG_ID)
);

/*==============================================================*/
/* Table: SAR_PRESTAMO                                          */
/*==============================================================*/
create table SAR_PRESTAMO
(
   PRE_ID               integer not null auto_increment,
   USU_ID               integer,
   EQU_ID               integer,
   USU_ID_SEC           integer,
   PRE_FECHA_INCIO      date not null,
   PRE_FECHA_FINAL      date,
   PRE_ESTADO           enum('p', 'a', 'r', 'f') not null,
   PRE_DETALLE          varchar(500),
   primary key (PRE_ID)
);

/*==============================================================*/
/* Table: SAR_TIPO_EQUIPO                                       */
/*==============================================================*/
create table SAR_TIPO_EQUIPO
(
   TEQ_ID               integer not null auto_increment,
   TEQ_CODIGO           varchar(250) not null,
   TEQ_DETALLE      varchar(500) not null,
   TEQ_ACTIVO           enum('s', 'n') not null,
   primary key (TEQ_ID)
);

/*==============================================================*/
/* Table: SAR_TIPO_USUARIO                                      */
/*==============================================================*/
create table SAR_TIPO_USUARIO
(
   TUS_ID               integer not null auto_increment,
   TUS_ROL              varchar(250) not null,
   primary key (TUS_ID)
);

/*==============================================================*/
/* Table: SAR_USUARIO                                           */
/*==============================================================*/
create table SAR_USUARIO
(
   USU_ID               integer not null auto_increment,
   TUS_ID               integer,
   USU_NOMBRES          varchar(100) not null,
   USU_APELLIDO_PATERNO varchar(20) not null,
   USU_APELLIDO_MATERNO varchar(20),
   USU_EMAIL            varchar(250) not null,
   USU_PASSWORD         varchar(250) not null,
   USU_ACTIVO           enum('s', 'n') not null,  
   USU_CARGO            varchar(50) not null,
   USU_DEPARTAMENTO         varchar(100) not null,
   USU_FECHA_NACIMIENTO date not null,
   primary key (USU_ID)
);

alter table SAR_EQUIPO add constraint FK_REFERENCE_4 foreign key (TEQ_ID)
      references SAR_TIPO_EQUIPO (TEQ_ID) ON DELETE CASCADE;

alter table SAR_LISTA_NEGRA add constraint FK_REFERENCE_6 foreign key (PRE_ID)
      references SAR_PRESTAMO (PRE_ID) ON DELETE CASCADE;

alter table SAR_PRESTAMO add constraint FK_REFERENCE_7 foreign key (USU_ID_SEC)
      references SAR_USUARIO (USU_ID) ON DELETE CASCADE;

alter table SAR_PRESTAMO add constraint FK_REFERENCE_2 foreign key (USU_ID)
      references SAR_USUARIO (USU_ID) ON DELETE CASCADE;

alter table SAR_PRESTAMO add constraint FK_REFERENCE_5 foreign key (EQU_ID)
      references SAR_EQUIPO (EQU_ID) ON DELETE CASCADE;

alter table SAR_USUARIO add constraint FK_REFERENCE_1 foreign key (TUS_ID)
      references SAR_TIPO_USUARIO (TUS_ID) ON DELETE CASCADE;

