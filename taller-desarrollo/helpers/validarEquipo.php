<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE VALIDAR LOS DATOS DE LA TABLA
 EQUIPO.
************************************************************************* */
class validarEquipo{ 
    //Principal: método que se utiliza para comprobar que exista fecha de ingreso al 
    //ingresar una fecha de desuso y que existan los valores
    public static function validarFechas($fechaIngreso, $fechaDesuso){
        return ($fechaDesuso>$fechaIngreso 
                && !is_null($fechaDesuso)
                && !is_null($fechaIngreso))
                ?1:0;
    } 
    /* 
       Principal: método que sirve para comprobar que los estados de los equipos corresponden a la definción de 
        valores
        a: activo
        i: inactivo
        r: reparación
    */
    public static function validarEstadoEquipo($estado_equipo){
        return (!is_null($estado_equipo)
                && ($estado_equipo==='a' || $estado_equipo==='i' || $estado_equipo==='r'))
                ?1:0;
    }
}