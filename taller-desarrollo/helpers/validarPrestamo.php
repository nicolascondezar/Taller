<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE VALIDAR LOS DATOS DE LA TABLA
 PRESTAMO
************************************************************************* */
class validarPrestamo{  
    public static function validarDetallePrestamo($pre_detalle)
    {
        return (!is_null($pre_detalle)
                && (strlen($pre_detalle)>3 && strlen($pre_detalle)<=700)
                && preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð -.,']+$/u",$pre_detalle)===1)
                ?1:0;
    }
/* 
       Principal: método que sirve para comprobar que los estados de los prestamos de equipos corresponden a la definición de 
        valores
        p: pendiente
        r: rechazado
        a: aceptado
        f: finalizado
        c: cancelado
    */
    public static function validarEstadoPrestamo($pr_estado)
    {
        return (!is_null($pr_estado)
                && ($pr_estado==='p' || $pr_estado==='r' || $pr_estado==='a' || $pr_estado==='f' || $pr_estado==='c'))
                ?1:0;
    }

    public static function validarFechasPrestamo($pre_fecha_inicio, $pre_fecha_final){
        return (!is_null($pre_fecha_final)
                && !is_null($pre_fecha_inicio)&&
                $pre_fecha_final>$pre_fecha_inicio 
                )
                ?1:0;
    } 
   
    public static function validarFechaInicioPrestamo($pre_fecha_inicio)
    {
        (!strpos($pre_fecha_inicio,'/'))
                ? $valores = explode('-', $pre_fecha_inicio)
                : $valores = explode('/', $pre_fecha_inicio);
        return (count($valores) === 3 
                && checkdate($valores[1], $valores[0], $valores[2]))
                ?1:0;
    }

    public static function validarFechaFinalPrestamo($pre_fecha_final)
    {
        (!strpos($pre_fecha_final,'/'))
                ? $valores = explode('-', $pre_fecha_final)
                : $valores = explode('/', $pre_fecha_final);
        return (count($valores) === 3 
                && checkdate($valores[1], $valores[0], $valores[2]))
                ?1:0;
    }

}
