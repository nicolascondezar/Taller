<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE PERMITE SUBIR LAS IMÁGENES DE LA LISTA NEGRA
************************************************************************* */
class Imagen{
    private $sourcePath; //ruta de origen nombre temporal
    private $nombreImagen; //nombre del archivo
    private $ext;//extensión de la imagen
    public function __construct($imagen, $nombreImagen)
    {
        $this->sourcePath = $imagen['tmp_name'];
        $this->nombreImagen = $nombreImagen;
        //guarda la extensión de la imagen ejemplo  image/jpg, se guarda jpg
        $this->ext = explode('/',$imagen['type'])[1];
    } 
    public function subirImagen(){
        //destino
        $targetPath = $_SERVER['DOCUMENT_ROOT'].'/asset/img/listanegra/'. $this->nombreImagen.".".$this->ext;
        //mueve el archivo al destino con su nombre correpindiente
        return (move_uploaded_file($this->sourcePath, $targetPath))
                ?1:0;
    } 
}
