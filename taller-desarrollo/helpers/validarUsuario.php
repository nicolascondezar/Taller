<?php
/*************************************************************************************/
//Descripción: clase que permite validar datos que corresponden al crud del usuario
/*************************************************************************************/
class validarUsuario{
    //Principal: valida el nombre de una persona
    public static function validarNombre($string){
        return (!is_null($string)
                && preg_match("/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ']+$/u", $string) === 1
                && (strlen($string)>=3 && strlen($string)<25 )
                )?1:0;       
    } 
    //Principal: validar fecha de nacimiento
    //valida rango de edad, que exista la fecha, que la fecha de nacimiento corresponda al rango
    //valida que el formato de la fecha exista
    public static function validarFecha($fechaNacimiento){
        return (!is_null($fechaNacimiento)
                && self::calcularEdad($fechaNacimiento)<=100 
                && self::calcularEdad($fechaNacimiento)>18
                && self::fechaNacMayorActual($fechaNacimiento) === 1
                && self::validarExistenciaFecha($fechaNacimiento) === 1
                )?1:0;
    }
     
    //valida que la fecha exista, ejemplo de error: 32/13/1989
    public static function validarExistenciaFecha($fecha){
        (!strpos($fecha,'/'))
                ? $valores = explode('-', $fecha)
                : $valores = explode('/', $fecha);
        return (count($valores) === 3 
                && checkdate($valores[1], $valores[0], $valores[2]))
                ?1:0;       
    }
    //valida que la fecha actual nacimiento no sea mayor que la fecha actual
    public static function fechaNacMayorActual($fecha){
        $fechaNacimiento = strtotime($fecha);
        $fechaNacimiento = date('Y-m-d h:i:s', $fechaNacimiento);
        $fechaNacimient = new DateTime($fechaNacimiento);
        $fechaActual = new DateTime(); 
        return ($fechaActual>=$fechaNacimient)?1:0;
    }
    //calcular edad de una persona
    public static function calcularEdad($fecha){   
        $fechaNacimiento = strtotime($fecha);
        $fechaNacimiento = date('Y-m-d h:i:s', $fechaNacimiento);
        $fechaNacimient = new DateTime($fechaNacimiento);
        $fechaActual = new DateTime();  
        return $fechaActual->diff($fechaNacimient)->format("%y");
    }

    //Principal: valida un hash, password: contraseña ingresada, $hash guardado en base de datos
    public static function validarHashPassword($password, $hash){
        return (!is_null($password)
                && !is_null($hash)
                && password_verify($password, $hash))
                ?1:0;
    } 
    //valida formato de password
    public static function validarFormatoPassword($password){
        return (!is_null($password)
                &&(preg_match("/^[0-9a-zA-Z]+$/u", $password) === 1)
                && (strlen($password)>=5 && strlen($password<12 )))
                ?1:0;
    }
    //Principal: valida rut chileno
    public static function validarRut($rut){
        return (!is_null($rut)
                &&(preg_match("/^[0-9]{7,8}+-[0-9kK]{1}$/u", $rut) === 1)
                && self::verificarDigitoVerificador($rut)==1)
                ?1:0;
    }
    public static function verificarDigitoVerificador($rut){
        //Paso 1: separamos el rut, número - dígito verificador
        $rut = explode('-',$rut);
        //Paso 2: se invierte el número
        $invertir= strrev($rut[0]); 
        //Paso 3: Ahora multiplicaremos cada uno de estos números por la siguiente serie: 2, 3, 4, 5, 6, 7 y si se acaba la serie, volvemos a empezar 2, 3, 4.
        $sum=0; 
        $j=2;  
        for ($i=0; $i < strlen($invertir) ; $i++) { 
                $sum = $sum + intval($invertir[$i])*$j;  
                $j=($j===7)? 2:($j+1);
        }  
        //Paso 4: se divido el resultado de la sumatoria por 11 y se guarda la parte entera del resto  
        $resto = intval($sum%11);  
        //paso 5: para obtener el digitoVerificador = 11 - $resto;
        $digitoVerificador = 11 - $resto;  
        return ($digitoVerificador==intval($rut[1]))?1:0;

    }
    //Principal: se valildan direcciones de correo electrónico con la sintaxis de RFC 822
    public static function validarEmail($email){
        return (!is_null($email)
                && filter_var($email, FILTER_VALIDATE_EMAIL))
                ?1:0;
    }
    //Principal: valida que el estado ingresado sea el correcto s: sí, n: no
    public static function validarEstado($estado){
        return (!is_null($estado) 
                && ($estado==='s' || $estado==='n'))
                ?1:0;
    }
}