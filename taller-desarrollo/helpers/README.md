# Clase validarUsuario 

1. Valida el nombre de una persona, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarNombre($string)` 
2. Validar fecha de nacimiento, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarFecha($fechaNacimiento)`  
3. Valida que el formato de la fecha exista, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarExistenciaFecha($fecha)` 
4. Valida que la fecha actual nacimiento no sea mayor que la fecha actual, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function fechaNacMayorActual($fecha)` 
5. calcular edad de una persona, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function calcularEdad($fecha)` 
6. Valida un hash, password: contraseña ingresada, hash guardado en base de datos, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarHashPassword($password, $hash)` 
7. Valida formato de rut, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarFormatoPassword($password)` 
8. Valida rut chileno, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarRut($rut)` 
9. Verifica el dígito verificador, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function verificarDigitoVerificador($rut)` 
10. Valilda las direcciones de correo electrónico con la sintaxis de RFC 822, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    `public static function validarEmail($email)` 
11. Valida que el estado ingresado sea el correcto , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
    ` public static function validarEstado($estado)` 
<br/><br/>
************************************

# Clase rutas 

1. Es el método que valida que la ruta actual , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/>
    `public function validarRutas()`    

2. Es el método para cargar las rutas válidas del sistema.<br/>
   `private function configurarRutas()`

3. Es el método para devolver las rutas guardadas en el sistema , <em>retorna `rutas` si es válido.</em>  
   `public function getRutas()`

4. Es el método para devolver las Url guardadas en el sistema , <em>retorna `url` si es válido.</em> <br/>
   `public function getUrl()`
<br/><br/>          
************************************

# Clase subirImagen

1. Es el método que guarda la extensión de la imagen ejemplo  image/jpg, se guarda jpg. <br/>
   `public function __construct($imagen, $nombreImagen)`

2. Es el método que mueve el archivo al destino con su nombre correpindiente , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/>
   `public function subirImagen()`   
<br/><br/>          
************************************

# Clase template

1. Es el método que retorna la plantilla.<br/> 
   `public function __construct($path, $data = [])`

2. Es el método que retorna la informacion de la plantilla , <em>retorna el `contenido` si es válido </em> <br/>
   `public function __toString()`
<br/><br/>          
************************************

# Clase validarEquipo 

1.  Es el método que se utiliza para comprobar que exista fecha de ingreso al ingresar una fecha de desuso y que existan los valores , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/>
   `public static function validarFechas($fechaIngreso, $fechaDesuso)`

2. Es el método que sirve para comprobar que los estados de los equipos corresponden a la definción de valores a: activo i: inactivo r: reparación , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/>
   `public static function validarEstadoEquipo($estado_equipo)`
<br/><br/>          
************************************

# Clase validarListaNegra

1. Es el método que valida el detalle de la lista negra la cual limita que caracteres se pueden ingresar , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/>
   `public static function validarDetalle($detalle)`

2. Es el método que valida la imagen, tamaño, tipo y caracteres en el nombre , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/> 
   `public static function validarImagen($archivo)`

3. Es el metodo que valida el formato de la imagen (jpg, jpeg, png) , <em>retorna un `1` si es válido o un `0` si es inválido.</em> <br/>
   `public static function validarFormato($type)`   


 
