<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE VALIDAR LOS DATOS DE LA TABLA
 LISTA NEGRA
************************************************************************* */
class validarListaNegra{  
    public static function validarDetalle($detalle){
        return (!is_null($detalle)
                && (strlen($detalle)>10 && strlen($detalle)<=500)
                && preg_match("/^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð .,']+$/u",$detalle)===1)
                ?1:0;
    }
    public static function validarImagen($archivo){ 
        return (!is_null($archivo) 
                && ($archivo['size']>0 && $archivo['size']<=5000000)
                && self::validarFormato($archivo['type'])
                && preg_match("/^[0-9a-zA-Z_]+$/u", explode('.',$archivo['name'])[0])===1
                )?1:0;
    }
    public static function validarFormato($type){
        $type =  strtolower($type);
        return ($type==='image/jpeg' || $type === 'image/jpg' || $type ==='image/png' )
                ?1:0;
    }
}