<?php
class Rutas{
    private $rutas= [];//rutas configuradas del sistema
    private $secundarias = [];
    private $url = [];//ruta actual
    public function __construct()
    {
        $this->configurarRutas();//se configuran las rutas del sistema    
        $uri= $_SERVER["REQUEST_URI"]; 
        $this->url =  explode("/",$uri); 
    }
    //método que valida que la ruta actual 
    public function validarRutas(){
        return (in_array( $this->url[1], $this->getRutas()) )
                ?1:0;
    }
    //método para cargar las rutas válidas del sistema
    private function configurarRutas(){
        $this->rutas = [
            "",
            "login",
            "intranet", 
            "autentificar",
            "salir"
        ]; 
        $this->secundarias = [
            "error"
        ];
    } 
    //método para devolver las rutas guardadas en el sistema
    public function getRutas(){
        return $this->rutas;
    } 
    public function getUrl2(){
        return (isset($this->url[2]))?$this->url[2]:"";
    }
    public function getUrl(){
        return $this->url[1];
    }
}