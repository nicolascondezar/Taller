<?php
class Template{
    private $contenido;//retorna el template
    public function __construct($path, $data = []){
        extract($data);
        ob_start();
        print_r(include($path));
        $this->contenido = ob_get_clean();
    }
    public function __toString()
    {
        return $this->contenido;
    }
}



 