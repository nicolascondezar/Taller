## Nombre del Proyecto
Sistema de gestión de asignación y reasignación de equipos computacionales de la Universidad del Sur.


## Software stack
El proyecto Sistema de gestión de asignación y reasignación de equipos computacionales de la Universidad del Sur. es una aplicación web que corre sobre el siguiente software:

Debian GNU/Linux 10 Buster 
Servidor WEB (Apache 2.4.3)
PHP 7.3 
Base de Datos MySQL 5
Git


## Configuraciones de Ejecución para Entorno de Desarrollo/Produccción

1-Crear una carpeta en en el equipo.

2-Luego ejecutar el siguiente comando en la terminal situandose en la direccion de la carpeta que creo anteriormente y luego clonar el proyecto:
git clone "ssh://git@gitlabtrans.face.ubiobio.cl:2526/17000309/taller-desarrollo.git".

3-luego de clonar el proyecto y dirigirse a la carpeta html, despues entrar en la carpeta archivo-entorno-docker y mover los siguientes archivos.

  - Dockerfile
  - docker-compose.yml
  - rest.conf
  
4- Descargar los archivos `Dockerfile`, `docker-compose.yml` y `rest.conf` en la carpeta raíz del proyecto.
  - __archivo Dockerfile__
  
![Dockerfile](./guia-instalacion-docker/dockerfile.png) 

  - __archivo docker-compose.yml__
  
![Dockerfile](./guia-instalacion-docker/docker-compose.png) 

  - __rest.conf__ 

![Dockerfile](./guia-instalacion-docker/configuracion_servidor_apache.png)

5- Crear carpeta `html` y `config`, la carpeta __html__ contiene los archivos del sistema y la carpeta __config__ contiene el php.ini del servidor.
 [alt text](./guia-instalacion-docker/estructura.png)

6- Ejecutar los siguientes pasos para instalación de entorno docker:
    - Ejecutar el comando `docker-compose pull` en la ráiz. 

    - Luego, ingresar el comando: `docker-compose build --no-cache`. 
    - Ingrese el comando `docker-compose up -d --force-recreate`.  
    - Liste los contenedores con el comando `docker ps -a` (aparece una lista con todos los contenedores, nombre, id, ip, etc), copiar id del servidor php.
![alt text](./guia-instalacion-docker/ejemplo-dockerps.png)
    - Utilice el comando: `docker cp <borre esto e ingrese el id de su contenedor apache>:/usr/local/etc/php ./conf` para copiar la carpeta que contiene la configuración de php (php.ini) en su entorno de trabajo ( también puede crear una variable para guardar el archivo de apache utilizando el mismo paso con la dirección del archivo dentro del SO debian del servidor apache) 
    - __NOTA__: para conocer el host de mysql (se puede usar con el apache y otras imágenes) y otros datos se recomienda usar el comando: `docker inspect <id_imagen_mysql>`
    ![alt text](./guia-instalacion-docker/inspect.png)

7- Al finalizar se instalarán las imágenes de apache y php7, MySql y PHPMyAdmin

8-. Ejecutar los script de creación de base de tablas, inserción y creación de vistas que están en la carpeta SQL en gitlab.

9- Ejecutar el script del archivo `alter.sql`, el cual contiene el script para agregar el atributo USU_RUT y actualiza los datos de una secretaria y de un administrador.

    - datos de secretaria usuario: klebretond@spotify.com, contraseña: Abc123def
    - datos de administrador usuario: jyukhtinq@cpanel.net, contraseña: Abc123def 
  
10- Actualizar los datos de conexcion de base de datos del archivo db.php ubicado en `/models/db.php`.


## Credenciales de Base de Datos y variables de ambiente


- datos de secretaria usuario: klebretond@spotify.com, contraseña: Abc123def
- datos de administrador usuario: jyukhtinq@cpanel.net, contraseña: Abc123def



******
# Pasos para la configuración del servidor 
******

1. En caso de tener alguna distribución de Linux en su equipo personal haga una conexión por SSH desde la consola.

    En el caso de Windows, descargar el software `PuTTY` e instalar. Tendrá una interfaz como la siguiente y debe agregar el `IP` 146.83.198.35 y el puerto `SSH` 1058.


    - __PuTTY__

    ![PuTTY](./Guia-instalacion-servidor/putty.png) 

2. Después de que se hayan agregado los datos ya mencionados aparecerá la siguiente consola, donde se tendrá que ingresar con su `usuario` usuario6 y `contraseña` U$er6. 

    
    - __Consola__

    ![Consola](./Guia-instalacion-servidor/consola.png)


3. Una vez listo se visualizará una consola como la siguiente.


     - __Consola__

    ![Consola](./Guia-instalacion-servidor/se_vera.png)

4. Luego de haber ingresado se tendrá que instalar `Apache`.

    
    - __Primer paso__

    Cambie el usuario a `root` con el comando `su -` e ingresando la clave `O4burto.2021$` y ejecute lo siguiente.

    `apt update`
    `apt install apache2 -y`

5. Ya finalizada la instalación de `Apache` se debe instalar `PHP`.

   
    - __Introduzca uno a uno los siguientes comandos__

    `apt update`
    `apt upgrade`
    `apt install php7.3 -y`
    
    - Nota: Para listar los módulos que se requiere instalar de PHP debe usar `apt search php7.3`.

     - __Ahora se instalaran los módulos necesarios__

    `apt install php7.3-common php7.3-mysql php7.3-xml php7.3-curl php7.3-gd php7.3-cli php7.3-dev php7.3-imap php7.3-mbstring php7.3-opcache php7.3-intl -y`
    - Reiniciar el apache con el comando `service apache2 restart`

    - Nota: Para listar los módulos PHP instalados se debe usar el comando `php -m`.

6. Posteriormente se procederá a instalar `GIT` para clonar el proyecto.


    - __Introduzca los siguientes comandos__

    `apt update`
    `apt install git -y`

7. en caso de no tener editor de texto, se debe instalar el editor de texto `nano` con los siguientes comandos:

    `apt update`
    `apt install nano`

8. Ingresar al directorio con el comando `cd /etc/apache2/sites-available`, luego editar el archivo `000-default.conf` con el comando `nano 000-default.conf`. Borre el contenido del archivo y copie el contenido del archivo `rest.conf` que está en la siguiente ruta en git `/guia-instalacion-docker/archivo-entorno-docker/`

9. Para finalizar se debe clonar el proyecto desde `GitLab`.


    - __Pasos a seguir__

    - Diríjase a la dirección var/www/html con el comando `cd /var/www/html`.
    - Dentro de la carpeta clonar el proyecto por HTTP.
    - Ingresar a la carpeta `taller-desarrollo` con el comando `cd taller-desarrollo` mover los archivos de a la carpeta `html` con el comando `mv .git controllers helpers middlewares sockets README.md asset index.php models views /var/www/html`.

    -Nota: Si se quiere eliminar la carpeta `taller-desarrollo` se debe usar el comando `rm -rfv taller-desarrollo`.

    - Para visualizar el proyecto poner en el navegador la URL `IP:Puerto_Apache`.

## Construido con

Bootstrap 4 - HTML, CSS, and JS Frontend Framework


## Contribuir al Proyecto

Por favor lea las instrucciones para contribuir al proyecto en CONTRIBUTING.md

## Ubicación de los archivos del entorno docker

Los archivos del entorno docker están ubicados en `./guia-instalacion-docker/archivo-entorno-docker` 

*********
## Direccitorios del proyecto  
*********
1. **asset**: Es el direcctorio donde se guardan los archivos estáticos, por ejemplo, archivos CSS, imágenes y los archivos JavaScript.
   
2. **controllers**: Es el directorio que contiene todos los controladores del sistema, los controladores actúan como intermediarios entre la vista y el modelo. 
   
3. **helpers**: Es el directorio que contiene los helpers, estos son juegos de funciones, ordenados por temática que nos ayudan en la realización de tareas habituales en las aplicaciones web. Algunos ejemplos de Helpers son los archivos que contienen funciones para validar datos, validar la base de datos, la creación tokens, validación de subida de archivos, etc.
   
4. **middlewares**: Este directorio contiene los middlewares del sistema, estos son archivos que contienen servicios y funciones comunes para las aplicaciones. En general, el middleware se encarga de las tareas de gestión de datos, servicios de aplicaciones, mensajería, autenticación y gestión de API.
    
5. **models**: Este directorio contiene los archivos de los modelos de datos de datos del sistema, estos archivos manejan la lógica de negocio del sistema.
   
6. **sockets**: Este directorio contiene los archivos para trabajar con los websocket del sistema, se utilizarán estas funciones para crear alertas en tiempo real.
   
7. **views**: Este directorio contiene las vistas, estas se encargan de renderizar los datos para que los puedan ver los usuarios.




