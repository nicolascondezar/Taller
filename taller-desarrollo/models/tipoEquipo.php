<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR EL MODELO DE LOS PERFILES 
 DE EQUIPOS QUE ESTÁN REGISTRADOS EN EL SISTEMA
************************************************************************* */
require_once("./models/db.php");
class tipoEquipo{
    private $teq_codigo;
    private $teq_detalle;
    private $teq_activo;
    private $connectDB;

    public function __construct()
    {
        $cpdo= new DB();
        $this->connectDB = $cpdo->conectar(); 
    }
    //crear perfiles de equipo
    public function crearTipoEquipo($teq_codigo, $teq_detalle, $teq_activo){
        try {
            $sentencia = $this->connectDB->prepare("INSERT 
                                                    INTO SAR_TIPO_EQUIPO(TEQ_CODIGO, TEQ_DETALLE, TEQ_ACTIVO)
                                                    VALUES (:TEQ_CODIGO, :TEQ_DETALLE, :TEQ_ACTIVO)");   
            $sentencia->bindParam(':TEQ_CODIGO', $teq_codigo);
            $sentencia->bindParam(':TEQ_DETALLE', $teq_detalle);
            $sentencia->bindParam(':TEQ_ACTIVO', $teq_activo); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }
    }


    //modificar datos de perfiles de equipo
    public function actualizarTipoEquipo($teq_id,$teq_codigo, $teq_detalle, $teq_activo){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_TIPO_EQUIPO
                                                SET TEQ_CODIGO = :TEQ_CODIGO, 
                                                    TEQ_DETALLE = :TEQ_DETALLE, 
                                                    TEQ_ACTIVO = :TEQ_ACTIVO,  
                                                WHERE TEQ_ID = :TEQ_ID");   
            $sentencia->bindParam(':TEQ_ID', $teq_id);
            $sentencia->bindParam(':TEQ_CODIGO', $teq_codigo);
            $sentencia->bindParam(':TEQ_DETALLE', $teq_detalle); 
            $sentencia->bindParam(':TEQ_ACTIVO', $teq_activo);
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }  
    } 

    //activar tipo equipo (perfil de equipo)
    public function desactivarTipoEquipo($teq_id){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_TIPO_EQUIPO 
                                                SET TEQ_ACTIVO = 'N'  
                                                WHERE TEQ_ID = :teq_id");    
            $sentencia->bindParam(':teq_id', $teq_id); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        } 
    }
    //desactivar tipo equipo (perfil de equipo)
    public function activarTipoEquipo($teq_id){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_TIPO_EQUIPO 
                                                SET TEQ_ACTIVO = 'S'  
                                                WHERE TEQ_ID = :teq_id");    
            $sentencia->bindParam(':teq_id', $teq_id); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        } 
    }


    //lista los tipos de equipos
    public function listarTiposEquipo(){
        $sql =  "SELECT * 
                 FROM SAR_TIPO_EQUIPO";
        return $this->connectDB->query($sql);
    }
    //obtener datos de un equipo por medio de id
    public function listarTipoEquipo($teq_id){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_TIPO_EQUIPO
                                                WHERE TEQ_ID = :teq_id AND TEQ_ACTIVO='S'");
        $sentencia->bindParam(':teq_id', $teq_id); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    }
}