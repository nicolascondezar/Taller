<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR EL MODELO DE LOS PRÉSTAMOS
 QUE EXISTEN EN EL SISTEMA
************************************************************************* */
//require_once("./models/db.php");

class Prestamo{
    private $usu_id;
    private $equ_id;
    private $pre_fecha_inicio; 
    private $pre_fecha_final; 
    private $pr_estado; 
    private $pre_detalle;
    private $connectDB;

    public function __construct($valor=null)
    { 
            $this->connectDB = $valor;    
    }
    //crear perfiles de los préstamos date('Y-m-d');
    public function crearPrestamo( $usu_id, $equ_id, $id_secretaria){
        try { 
            $sql = "INSERT 
                    INTO SAR_PRESTAMO(USU_ID, EQU_ID, PRE_FECHA_INCIO, USU_ID_SEC, PRE_ESTADO) 
                    VALUES (?,?,?,?,?)";
            $stmt= $this->connectDB->prepare($sql);
            $stmt->execute([$usu_id, $equ_id, date("Y-m-d"), $id_secretaria, "p"]);
            return 1;
        } catch (\Throwable $th) {
            return 0;
        }
    }


    //modificar datos de perfiles de equipo
    public function actualizarPrestamo($pre_id, $usu_id, $equ_id, $pre_fecha_inicio, $pre_fecha_final, $pre_estado, $pre_detalle){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_PRESTAMO
                                                SET USU_ID = :USU_ID, 
                                                    EQU_ID = :EQU_ID, 
                                                    PRE_FECHA_INICIO = :PRE_FECHA_INICIO, 
                                                    PRE_FECHA_FINAL = :PRE_FECHA_FINAL, 
                                                    PRE_ESTADO = :PRE_ESTADO, 
                                                    PRE_DETALLE = :PRE_DETALLE  
                                                WHERE PRE_ID = :PRE_ID");  
            $sentencia->bindParam(':PRE_ID', $pre_id);                                  
            $sentencia->bindParam(':USU_ID', $usu_id);
            $sentencia->bindParam(':EQU_ID', $equ_id);
            $sentencia->bindParam(':PRE_FECHA_INICIO', $pre_fecha_inicio); 
            $sentencia->bindParam(':PRE_FECHA_FINAL', $pre_fecha_final);
            $sentencia->bindParam(':PRE_ESTADO', $pre_estado);
            $sentencia->bindParam(':PRE_DETALLE', $pre_detalle);
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }  
    } 
 
  public function actualizarPrestamoSecretaria( $usu_id, $equ_id, $prestamo){
    try { 
        $sql = "UPDATE SAR_PRESTAMO
                SET USU_ID=?, EQU_ID=?
                WHERE PRE_ID=?";
        $stmt= $this->connectDB->prepare($sql);
        $stmt->execute([$usu_id, $equ_id, $prestamo]);
        return 1;
    } catch (\Throwable $th) {
        return 0;
    }
}

  /*UPDATE SAR_PRESTAMO 
SET PRE_ESTADO = 'r', PRE_DETALLE =:PRE_DETALLE
WHERE PRE_ID = :PRE_ID*/
    public function actualizarPrestamoAdmin( $id_prestamo,$detalle){
        try { 
            $sql = "UPDATE SAR_PRESTAMO 
            SET PRE_ESTADO = 'r', PRE_DETALLE = ?
            WHERE PRE_ID = ?";
            $stmt= $this->connectDB->prepare($sql);
            $stmt->execute([$detalle,$id_prestamo]);
            return 1;
        } catch (\Throwable $th) {
            return 0;
    }
    }

    public function actualizarPrestamoAceptadoAdmin( $id_prestamo){
        try { 
            $sql = "UPDATE SAR_PRESTAMO 
            SET PRE_ESTADO = 'a'
            WHERE PRE_ID = ?";
            $stmt= $this->connectDB->prepare($sql);
            $stmt->execute([$id_prestamo]);
            return 1;
        } catch (\Throwable $th) {
            return 0;
    }
    }

    //lista los prestamos
    public function listarPrestamo(){
        $sql =  "SELECT * 
                 FROM SAR_PRESTAMO";
        return $this->connectDB->query($sql);
    }
    public function listarPrestamosActivos($id){
        $sql = "SELECT P.PRE_ID, E.EQU_ID, T.TEQ_CODIGO, U.USU_RUT, U.USU_NOMBRES, U.USU_APELLIDO_PATERNO, P.PRE_FECHA_INCIO, P.PRE_ESTADO 
                FROM SAR_PRESTAMO P, SAR_USUARIO U, SAR_TIPO_EQUIPO T, SAR_EQUIPO E 
                WHERE P.USU_ID = U.USU_ID AND P.USU_ID_SEC=$id AND P.EQU_ID = E.EQU_ID AND E.TEQ_ID = T.TEQ_ID	
                ORDER BY P.PRE_ID ASC"; 
        return $this->connectDB->query($sql);
    }
    public function listarPrestamosActivosAdm($id){
        $sql = "SELECT P.PRE_ID, E.EQU_ID, T.TEQ_CODIGO, U.USU_RUT, U.USU_NOMBRES, U.USU_APELLIDO_PATERNO, P.PRE_FECHA_INCIO, P.PRE_FECHA_FINAL,P.PRE_ESTADO 
                FROM SAR_PRESTAMO P, SAR_USUARIO U, SAR_TIPO_EQUIPO T, SAR_EQUIPO E 
                WHERE P.USU_ID = U.USU_ID AND P.EQU_ID = E.EQU_ID AND E.TEQ_ID = T.TEQ_ID AND P.PRE_ESTADO = 'p'
                ORDER BY P.PRE_ID ASC"; 
        return $this->connectDB->query($sql);
    }

    public function verPrestamosId($id){
        $sql = "SELECT U.USU_NOMBRES, U.USU_APELLIDO_PATERNO, U.USU_APELLIDO_MATERNO, U.USU_RUT, U.USU_DEPARTAMENTO, U.USU_CARGO, T.TEQ_CODIGO, T.TEQ_DETALLE, P.PRE_DETALLE	
                FROM SAR_PRESTAMO P, SAR_USUARIO U, SAR_EQUIPO E, SAR_TIPO_EQUIPO T
                WHERE P.USU_ID = U.USU_ID AND P.PRE_ID=:id AND P.EQU_ID = E.EQU_ID AND E.TEQ_ID = T.TEQ_ID"; 
        $valor = $this->connectDB->prepare($sql);
        $valor->bindParam(':id', $id); 
        $valor->execute();
        return $valor->fetchAll();
    }
    
    //obtener datos de un préstamo
    public function listarPrestamos($pre_id){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_PRESTAMO
                                                WHERE PRE_ID = :pre_id");
        $sentencia->bindParam(':pre_id', $pre_id); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    } 
}