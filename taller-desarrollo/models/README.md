# Clase DB

1. Es el método que conecta con la base de datos, <em>retorna una `connectDB` si es válido o un `null` si es inválido</em> <br/>
   `public function conectar()`
<br/><br/>
************************************

# Clase equipo

1. Es el método que ingresa los equipos , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function crearEquipo($teq_id, $equ_fecha_ingreso, $equ_fecha_desuso, $equ_estado)` 

2. Es el método que modifica los datos del equipo , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function actualizarEquipo($equ_id, $teq_id, $equ_fecha_ingreso, $equ_fecha_desuso, $equ_estado)`

3. Es el método que lista los equipos , <em>retorna una `query($sql)` si es válido </em> <br/>
   `public function listarEquipos()`

4. Es el método que obtiene los datos de un equipo mediante la id , <em>retorna un `fetchAll()` si es válido </em> <br/>
   `public function listarEquipo($equ_id)`
<br/><br/>
************************************

# Clase listanegra

1. Es el método que crea la lista negra , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function crearListaNegra($pre_id, $lng_detalle, $lng_imagen)`

2. Es el método que modifica los datos de la lista negra , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function actualizarListaNegra($lng_id, $pre_id, $lng_detalle, $lng_imagen)`

3. Es el método que lista los datos de la lista negra , <em>retorna una `query($sql)` si es válido </em> <br/>
   `public function listarListaNegra()`

4. Es el método que obtiene los datos de una lista negra , <em>retorna un `fetchAll()` si es válido </em> <br/>
   `public function listarTipoEquipo($lng_id)`
<br/><br/>
************************************

# Clase prestamo

1. Es el método que crea prerfiles de prestamos , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function crearPrestamo( $usu_id, $equ_id, $pre_fecha_inicio, $pre_fecha_final, $pre_estado, $pre_detalle)`

2. Es el método que modifica los datos de perfiles de prestamos , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function actualizarPrestamo($pre_id, $usu_id, $equ_id, $pre_fecha_inicio, $pre_fecha_final, $pre_estado, $pre_detalle)`

3. Es el método que lista los prestamos , <em>retorna una `query($sql)` si es válido </em> <br/>
   `public function listarTiposEquipo()`

4. Es el metodo que obtiene los datos de los prestamos , <em>retorna un `fetchAll()` si es válido </em> <br/>
   `public function listarTipoEquipo($pre_id)`
<br/><br/>
************************************

# Clase tipoEquipo

1. Es el método que crea prerfiles de equipos , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function crearTipoEquipo($teq_codigo, $teq_detalle, $teq_activo)`

2. Es el método que modifica los datos de perfiles de equipo , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function actualizarTipoEquipo($teq_id,$teq_codigo, $teq_detalle, $teq_activo)`

3. Es el metodo que activa el tipo de equipo en el perfil de equipo, <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function desactivarTipoEquipo($teq_id)`
   
4. Es el metodo que desactiva el tipo de equipo en el perfil de equipo , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function activarTipoEquipo($teq_id)`

5. Es el método que lista los tipos de equipos , <em>retorna una `query($sql)` si es válido </em> <br/>
   `public function listarTiposEquipo()`

6. Es el metodo que obtiene los datos de los prestamos , <em>retorna un `fetchAll()` si es válido </em> <br/>
   `public function listarTipoEquipo($teq_id)`
<br/><br/>
************************************

# Clase tipoUsuario 

1. Es el método que crear el tipo de usuario(rol) , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function crearTipoUsuario($tus_rol)`

2. Es el método que lista los roles de usuario , <em>retorna una `query($sql)` si es válido </em> <br/>
   `public function listarRolesUsuario()`
<br/><br/>
************************************

# Clase Usuario

1. Es el método que crea usuarios , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function crearUsuario($tus_id, $usu_nombres, $usu_apellido_paterno, $usu_apellido_materno, $usu_email, $usu_password, $usu_activo, $usu_cargo, $usu_departamento, $usu_fecha_nacimiento)`

2. Es el método que modifica los datos de usuario , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function actualizarUsuario($usu_id, $tus_id, $usu_nombres, $usu_apellido_paterno, $usu_apellido_materno, $usu_email, $usu_password, $usu_activo, $usu_cargo, $usu_departamento, $usu_fecha_nacimiento)`

3. Es el metodo que desactiva usuarios , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function desactivarUsuario($usu_id)`
   
4. Es el metodo que activa usuario , <em>retorna un `1` si es válido o un `0` si es inválido</em> <br/>
   `public function activarUsuario($usu_id)`

5. Es el método que lista a los  usuarios , <em>retorna una `query($sql)` si es válido </em> <br/>
   `public function listarUsuarios()`

6. Es el metodo que estra los datos de un usuario filtrado por su id , <em>retorna un `fetchAll()` si es válido </em> <br/>
   `public function listarUsuario($usu_id)`