<?php
/*************************************************************************************/
//Descripción: clase que permite configurar la base de datos   
/*************************************************************************************/
class DB{
    
    private $host="172.19.0.1";
    private $user="root";
    private $password="root";
    private $database="test_db";
    private $port=3308;
    public $connectDB =null; //atributo que guarda la conexión a la base de datos
    //método de conexión a base de datos
    public function conectar(){      
            try {
                
                $this->connectDB = new PDO("mysql:host=$this->host;port=$this->port;dbname=$this->database", $this->user, $this->password);
                $this->connectDB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $this->connectDB;
            } catch (PDOException $e) {  
                echo $e;
                return null; 
            }
    } 
}
