<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR EL MODELO DE LOS USUARIOS
 QUE ESTÁN REGISTRADOS EN EL SISTEMA, EL CRUD DE USUARIO
************************************************************************* */
 
class Usuario{
    private $tus_id;
    private $usu_nombres;
    private $usu_apellido_paterno;
    private $usu_apellido_materno;
    private $usu_email;
    private $usu_password;
    private $usu_activo;
    private $usu_cargo;
    private $usu_departamento;
    private $usu_fecha_nacimiento;
    private $connectDB;
    public function __construct($valor = null)
    { 
        $this->connectDB = $valor; 
    }
    //crear usuario
    public function crearUsuario($tus_id, $usu_nombres, $usu_apellido_paterno, $usu_apellido_materno, $usu_email, $usu_password, $usu_activo, $usu_cargo, $usu_departamento, $usu_fecha_nacimiento){
        try {
            $sentencia = $this->connectDB->prepare("INSERT 
                                                INTO SAR_USUARIO(TUS_ID, USU_NOMBRES, USU_APELLIDO_PATERNO, USU_APELLIDO_MATERNO, USU_EMAIL, USU_PASSWORD, USU_ACTIVO, USU_CARGO, USU_DEPARTAMENTO, USU_FECHA_NACIMIENTO) 
                                                VALUES (:tus_id, :usu_nombres, :usu_apellido_paterno, :usu_apellido_materno, :usu_email, :usu_password, :usu_activo, :usu_cargo, :usu_departamento, :usu_fecha_nacimiento)");   
            $sentencia->bindParam(':tus_id', $tus_id);
            $sentencia->bindParam(':usu_nombres', $usu_nombres);
            $sentencia->bindParam(':usu_apellido_paterno', $usu_apellido_paterno);
            $sentencia->bindParam(':usu_apellido_materno', $usu_apellido_materno);
            $sentencia->bindParam(':usu_email', $usu_email);
            $sentencia->bindParam(':usu_password', $usu_password);
            $sentencia->bindParam(':usu_activo', $usu_activo);
            $sentencia->bindParam(':usu_cargo', $usu_cargo);
            $sentencia->bindParam(':usu_departamento', $usu_departamento);
            $sentencia->bindParam(':usu_fecha_nacimiento', $usu_fecha_nacimiento);
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }
    } 
    //modificar datos de usuario
    public function actualizarUsuario($usu_id, $tus_id, $usu_nombres, $usu_apellido_paterno, $usu_apellido_materno, $usu_email, $usu_password, $usu_activo, $usu_cargo, $usu_departamento, $usu_fecha_nacimiento){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_USUARIO  
                                                SET TUS_ID = :tus_id, 
                                                    USU_NOMBRES = :usu_nombres, 
                                                    USU_APELLIDO_PATERNO = :usu_apellido_paterno, 
                                                    USU_APELLIDO_MATERNO = :usu_apellido_materno, 
                                                    USU_EMAIL = :usu_email, 
                                                    USU_PASSWORD = :usu_password, 
                                                    USU_ACTIVO = :usu_activo, 
                                                    USU_CARGO = :usu_cargo, 
                                                    USU_DEPARTAMENTO = :usu_departamento, 
                                                    USU_FECHA_NACIMIENTO = :usu_fecha_nacimiento
                                                WHERE USU_ID = :usu_id");   
            $sentencia->bindParam(':tus_id', $tus_id);
            $sentencia->bindParam(':usu_nombres', $usu_nombres);
            $sentencia->bindParam(':usu_apellido_paterno', $usu_apellido_paterno);
            $sentencia->bindParam(':usu_apellido_materno', $usu_apellido_materno);
            $sentencia->bindParam(':usu_email', $usu_email);
            $sentencia->bindParam(':usu_password', $usu_password);
            $sentencia->bindParam(':usu_activo', $usu_activo);
            $sentencia->bindParam(':usu_cargo', $usu_cargo);
            $sentencia->bindParam(':usu_departamento', $usu_departamento);
            $sentencia->bindParam(':usu_fecha_nacimiento', $usu_fecha_nacimiento);
            $sentencia->bindParam(':usu_id', $usu_id); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }  
    } 
    //desactivar usuario
    public function desactivarUsuario($usu_id){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_USUARIO  
                                                SET USU_ACTIVO = 'N'  
                                                WHERE USU_ID = :usu_id");    
            $sentencia->bindParam(':usu_id', $usu_id); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        } 
    }
    //activar usuario
    public function activarUsuario($usu_id){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_USUARIO  
                                                SET USU_ACTIVO = 'S'  
                                                WHERE USU_ID = :usu_id");    
            $sentencia->bindParam(':usu_id', $usu_id); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        } 
    }
    //listar usuarios
    public function listarUsuarios(){
        $sql =  "SELECT * 
                 FROM SAR_USUARIO
                 WHERE USU_ACTIVO='S'";
        return $this->connectDB->query($sql);
    }

    //muestra los datos de un usuario filtrado por su id
    public function listarUsuario($usu_id){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_USUARIO 
                                                WHERE USU_ID = :usu_id AND USU_ACTIVO='s'");
        $sentencia->bindParam(':usu_id', $usu_id); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    }

    //listar usuario por rut

    public function listarUsuarioRut($usu_rut){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_USUARIO 
                                                WHERE USU_RUT = :usu_rut AND USU_ACTIVO='S'");
        $sentencia->bindParam(':usu_rut', $usu_rut); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    }
    //muestra los datos de un usuario filtrado por su email
    public function listarUsuarioEmail($usu_email){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_USUARIO 
                                                WHERE USU_EMAIL = :usu_email AND USU_ACTIVO='S'");
        $sentencia->bindParam(':usu_email', $usu_email); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    }
    
}


