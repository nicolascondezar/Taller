<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR LA TABLA QUE MOSTRARA LOS DATOS 
************************************************************************* */
require_once("./models/db.php");
class Tabla{
    var $tablas;
    public function __construct(){
        $cpdo= new DB();
        $this->connectDB = $cpdo->conectar(); 
    }
    function mostrar(){
        $sql="SELECT * FROM datatable";
        $resultado = $this->acceso->query($sql);
        $this->tablas = $resultado->fetch_all(MYSQLI_ASSOC);
        return $this->tablas;
    }
}