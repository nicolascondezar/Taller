<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR EL MODELO DE La lista negra
 QUE EXISTEN EN EL SISTEMA
************************************************************************* */
require_once("./models/db.php");
class prestamo{
    private $pre_id;
    private $lng_detalle; 
    private $lng_imagen;
    private $connectDB;

    public function __construct()
    {
        $cpdo= new DB();
        $this->connectDB = $cpdo->conectar(); 
    }
    //crear lista negra
    public function crearListaNegra($pre_id, $lng_detalle, $lng_imagen){
        try {
            $sentencia = $this->connectDB->prepare("INSERT
                                                    INTO SAR_LISTA_NEGRA(PRE_ID, LNG_DETALLE, LNG_IMAGEN)
                                                    VALUES (:PRE_ID, :LNG_DETALLE, :LNG_IMAGEN)");   
            $sentencia->bindParam(':PRE_ID', $pre_id);
            $sentencia->bindParam(':LNG_DETALLE', $lng_detalle);
            $sentencia->bindParam(':LNG_IMAGEN', $lng_imagen); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }
    }


    //modificar datos de LISTA NEGRA
    public function actualizarListaNegra($lng_id, $pre_id, $lng_detalle, $lng_imagen){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_LISTA_NEGRA
                                                SET PRE_ID = :PRE_ID, 
                                                    LNG_DETALLE = :LNG_DETALLE,
                                                    LNG_IMAGEN = :LNG_IMAGEN 
                                                WHERE LNG_ID = :LNG_ID");  
            $sentencia->bindParam(':LNG_ID', $lng_id);                                  
            $sentencia->bindParam(':PRE_ID', $pre_id);
            $sentencia->bindParam(':LNG_DETALLE', $lng_detalle);
            $sentencia->bindParam(':LNG_IMAGEN', $lng_imagen);
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }  
    } 

  
  


    //listar lista negra
    public function listarListaNegra(){
        $sql =  "SELECT * 
                 FROM SAR_LISTA_NEGRA";
        return $this->connectDB->query($sql);
    }
    //obtener datos de lista negra
    public function listarTipoEquipo($lng_id){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_LISTA_NEGRA
                                                WHERE LNG_ID = :lng_id");
        $sentencia->bindParam(':lng_id', $lng_id); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    } 
}