<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR LOS  
 EQUIPOS QUE ESTÁN REGISTRADOS EN EL SISTEMA
************************************************************************* */
require_once("./models/db.php");
class Equipo{
    private $teq_id; 
    private $equ_fecha_ingreso;
    private $equ_fecha_desuso;
    private $equ_estado;
    private $connectDB;

    public function __construct()
    {
        $cpdo= new DB();
        $this->connectDB = $cpdo->conectar(); 
    }
    //ingresar equipo
    public function crearEquipo($teq_id, $equ_fecha_ingreso, $equ_fecha_desuso, $equ_estado){
        try {
            $sentencia = $this->connectDB->prepare("INSERT
                                                    INTO SAR_EQUIPO(TEQ_ID, EQU_FECHA_INGRESO, EQU_FECHA_DESUSO, EQU_ESTADO)
                                                    VALUES (:TEQ_ID, :EQU_FECHA_INGRESO, :EQU_FECHA_DESUSO, :EQU_ESTADO)");   
            $sentencia->bindParam(':TEQ_ID', $teq_id);
            $sentencia->bindParam(':EQU_FECHA_INGRESO', $equ_fecha_ingreso);
            $sentencia->bindParam(':EQU_FECHA_DESUSO', $equ_fecha_desuso); 
            $sentencia->bindParam(':EQU_ESTADO', $equ_estado); 
            return ($sentencia->execute())?1:0;
            
        } catch (\Throwable $th) {
            return 0;
        }
    }


    //modificar datos de equipo
    public function actualizarEquipo($equ_id, $teq_id, $equ_fecha_ingreso, $equ_fecha_desuso, $equ_estado){
        try {
            $sentencia = $this->connectDB->prepare("UPDATE SAR_EQUIPO
                                                    SET TEQ_ID = :TEQ_ID, 
                                                        EQU_FECHA_INGRESO = :EQU_FECHA_INGRESO, 
                                                        EQU_FECHA_DESUSO = :EQU_FECHA_DESUSO, 
                                                        EQU_ESTADO = :EQU_ESTADO  
                                                    WHERE EQU_ID = :EQU_ID");   
            $sentencia->bindParam(':EQU_ID', $equ_id);
            $sentencia->bindParam(':TEQ_ID', $teq_id);
            $sentencia->bindParam(':EQU_FECHA_INGRESO', $equ_fecha_ingreso);
            $sentencia->bindParam(':EQU_FECHA_DESUSO', $equ_fecha_desuso); 
            $sentencia->bindParam(':EQU_ESTADO', $equ_estado); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }  
    } 

    


    //lista los de equipos
    public function listarEquipos(){
        $sql =  "SELECT * 
                 FROM SAR_EQUIPO";
        return $this->connectDB->query($sql);
    }

    public function listarEquiposActivos(){
        $sql = "SELECT S.EQU_ID, S.TEQ_ID, T.TEQ_CODIGO, T.TEQ_DETALLE 
        FROM SAR_EQUIPO S, SAR_TIPO_EQUIPO T 
        WHERE EQU_ESTADO = 'a' AND S.TEQ_ID = T.TEQ_ID AND
              S.EQU_ID NOT IN ( SELECT P.EQU_ID
                                FROM SAR_PRESTAMO P
                                  WHERE S.EQU_ID = P.EQU_ID AND (P.PRE_ESTADO='p' OR P.PRE_ESTADO='a'))	";
        return $this->connectDB->query($sql);
    }
    public function listarEquiposActivosadm(){
        $sql = "SELECT S.EQU_ID, S.TEQ_ID, T.TEQ_CODIGO, T.TEQ_DETALLE
        FROM SAR_EQUIPO S, SAR_TIPO_EQUIPO T 
        WHERE EQU_ESTADO = 'a' AND S.TEQ_ID = T.TEQ_ID AND
              S.EQU_ID NOT IN ( SELECT P.EQU_ID
                                FROM SAR_PRESTAMO P
                                  WHERE S.EQU_ID = P.EQU_ID AND (P.PRE_ESTADO='p' OR P.PRE_ESTADO='a'))	";
        return $this->connectDB->query($sql);
    }
    //obtener datos de un equipo por medio de id
    public function listarEquipo($equ_id){
        $sentencia = $this->connectDB->prepare("SELECT * 
                                                FROM SAR_EQUIPO
                                                WHERE EQU_ID = :equ_id");
        $sentencia->bindParam(':equ_id', $equ_id); 
        $sentencia->execute();
        return $sentencia->fetchAll();
    }
}