<?php
/*********************************************************************** 
 DETALLE: ES LA CLASE QUE SE ENCARGA DE MANEJAR EL MODELO DE LOS ROLES DE USUARIOS
 QUE EXISTEN EN EL SISTEMA
************************************************************************* */
require_once("./models/db.php");
class tipoUsuario{
    private $tus_rol;
    private $connectDB;
    public function __construct()
    {
        $cpdo= new DB();
        $this->connectDB = $cpdo->conectar(); 
    }
    public function crearTipoUsuario($tus_rol){
        try {
            $sentencia = $this->connectDB->prepare("INSERT 
                                                    INTO SAR_TIPO_USUARIO(TUS_ROL) 
                                                    VALUES (:tus_rol)");   
            $sentencia->bindParam(':tus_rol', $tus_rol); 
            return ($sentencia->execute())?1:0;
        } catch (\Throwable $th) {
            return 0;
        }
    }
    public function listarRolesUsuario(){
        $sql =  "SELECT * 
                 FROM SAR_TIPO_USUARIO";
        return $this->connectDB->query($sql);
    }
    
}