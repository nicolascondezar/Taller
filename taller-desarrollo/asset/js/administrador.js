
$(document).ready(function () { 
  
    const btnModificarDisable = document.getElementById("btnModificar");   
   // btnModificarDisable.disabled = true;
    $("#myModal").on('hidden.bs.modal', function () {
        location.reload();
    });
    $("#ModalRechazado").on('hidden.bs.modal', function () {
        location.reload();
    });
    //crear petición prestamo
  
   
    //modificar petición de préstamo
   /* $("#crearprestamo").change(function (e) {  
        e.preventDefault(); 
        const respuesta = document.getElementById("respuesta").value;
        const btnModificarDisable = document.getElementById("btnModificar");
        var expreg = /^[0-9a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð -.,']+$/ 
        if(expreg.test(respuesta)){ 
            btnModificarDisable.disabled= false;
        }else{
            btnModificarDisable.disabled= true;
        }
        
        });*/
    
//se agrega un valor mediante una forma dinamica al modal
    $("#botones2 .modificar").click(function (e) { 
        e.preventDefault();
        var btnValor = $(this).val() 
       
        $("#ModalRechazado").on('show.bs.modal', function () {
            //se traen los elementos del formulario
            let id = document.getElementById("id_prestamo");            
            id.setAttribute("value", btnValor);
            
            
        });  
    });
  
    $("#btnModificar").click(function (e) { 
        e.preventDefault();

        const idEquipo = document.getElementById('id_prestamo').value;
        const respuesta = document.getElementById('respuesta').value;
        $.ajax({
            // la URL para la petición
            url : '../../controllers/modificarPrestamoAdmin.php', 
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data : {
                idEquipo,
                respuesta
              
            },
        
            // especifica si será una petición POST o GET
            type : 'POST',
        
            // el tipo de información que se espera de respuesta
            dataType : 'json',
        
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success : function(json) {
                console.log("ingreso")
                /*$('<h1/>').text(json.title).appendTo('body');
                $('<div class="content"/>')
                /*    .html(json.html).appendTo('body');*/
               let mensaje = document.getElementById("mmensaje"); 
                if(json.error ==0){
                    mensaje.className = "alert alert-success"
                    mensaje.innerHTML = `${json.mensaje}`
                    
                }else{
                    mensaje.className = "alert alert-danger"
                    mensaje.innerHTML = `${json.mensaje}`
                                    
                }
                
           
            } 
        });

        
    });

    //Boton aceptar prestamo

    $("#botones2 .aceptar").click(function (e) { 
        e.preventDefault();
        //const idEquipo = document.getElementById('id_prestamo').value;
        var idPrestamo = $(this).val(); 
        //console.log(btnValor);
        $.ajax({
            // la URL para la petición
            url : '../../controllers/modificarPrestamoAdminAceptado.php', 
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data : {
                idPrestamo
              
            },
        
            // especifica si será una petición POST o GET
            type : 'POST',
        
            // el tipo de información que se espera de respuesta
            dataType : 'json',
        
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success : function(json) {
                
                location.reload();                  
            }
                
        });

        
    });

});


