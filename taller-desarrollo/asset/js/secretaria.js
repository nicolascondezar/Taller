/***********************************************
 * Descripción: En este documento están las funciones
 * que sirven para trabajar con el crud mediante el 
 * uso de jquery y ajax para cargar datos y aplicar 
 * acciones del módulo de la secretaria 
***********************************************/

$(document).ready(function () { 
    const btnCrearDisable = document.getElementById("btnCrear");
    const btnModificarDisable = document.getElementById("btnModificar");
    btnCrearDisable.disabled= true;
    btnModificarDisable.disabled = false;
    $("#myModal").on('hidden.bs.modal', function () {
        location.reload();
    });
    $("#myModalModificar").on('hidden.bs.modal', function () {
        location.reload();
    });
    //crear petición prestamo
    $("#crut").change(function (e) {  
        e.preventDefault(); 
        const crut = document.getElementById("crut").value;
        const btnCrearDisable = document.getElementById("btnCrear");
        var expreg = /^[0-9]+[-|‐]{1}[0-9kK]{1}$/ 
        if(expreg.test(crut)){ 
            btnCrearDisable.disabled= false;
        }else{
            btnCrearDisable.disabled= true;
        }
    });
    $("#btnCrear").click(function (e) {  
        e.preventDefault(); 
        const mensajeb = document.getElementById("cmensaje");
        mensajeb.innerHTML= "";
        mensajeb.removeAttribute("class")
        const crut = document.getElementById("crut").value;
        const csecretaria = document.getElementById("csecretaria").value;
        const cid_equipo = document.getElementById("cequipo").value; 
        $.ajax({
            // la URL para la petición
            url : '../../controllers/crearPrestamo.php', 
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data : {
                crut,
                csecretaria,
                cid_equipo
            },
        
            // especifica si será una petición POST o GET
            type : 'POST',
        
            // el tipo de información que se espera de respuesta
            dataType : 'json',
        
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success : function(json) {
                /*$('<h1/>').text(json.title).appendTo('body');
                $('<div class="content"/>')
                    .html(json.html).appendTo('body');*/ 
               
                let mensaje = document.getElementById("cmensaje"); 
                if(json.error ==0){
                    mensaje.className = "alert alert-success"
                    mensaje.innerHTML = `${json.mensaje}`
                    
                }else{
                    mensaje.className = "alert alert-danger"
                    mensaje.innerHTML = `${json.mensaje}`
                    
                } 
            } 
        });
    });
    //modificar petición de préstamo

    

    $("#botones .modificar").click(function (e) { 
        e.preventDefault();
        var btnValor = $(this).val() 
        $("#myModalModificar").on('show.bs.modal', function () {
            //se traen los elementos del formulario
            const crut      = document.getElementById("mrut");//input rut 
            const cprestamo = document.getElementById("mprestamo");//input oculto con id de préstamo
            
            //elementos enviados por botón
           
            partes = btnValor.split("/"); 
            //asignación de valores
            crut.value= partes[1]; 
            let aux;
            switch (partes[3]) {
                case "equipo1":
                    aux="procesador amd ryzen 5 2400g, 8 gb ram";
                    break;
                case "equipo2":
                    aux = "procesador amd ryzen 3 2200g, 8 gb ram";
                    break;
                case "equipo 3":
                    aux= "procesador amd ryzen 5 3400g, 16 gb ram";
                    break
            }  
            
            //se agrega la opción registada con la propiedad selected en el selector
            $('#mequipo').append($('<option>', {
                value: partes[2],
                selected: true,
                text: `${partes[2]} - ${partes[3]} - ${aux}`
            }));

            //se agrega el id del préstamo 
            cprestamo.setAttribute("value", partes[0]);
    
        });  
    });
    //modificar
    $("#mrut").change(function (e) {  
        e.preventDefault(); 
        const mrut = document.getElementById("mrut").value;
        const btnDisable = document.getElementById("btnModificar");
        var expreg = /^[0-9]+[-|‐]{1}[0-9kK]{1}$/ 
        if(expreg.test(mrut)){ 
            btnDisable.disabled= false;
        }else{
            btnDisable.disabled= true;
        }
    });
    $("#btnModificar").click(function (e) { 
        e.preventDefault();
        var mrut = document.getElementById("mrut").value;
        var mequipo = document.getElementById("mequipo").value;
        var mprestamo = document.getElementById("mprestamo").value;
        $.ajax({
            // la URL para la petición
            url : '../../controllers/modificarPrestamo.php', 
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data : {
                mrut, //rut usuario asociado al préstamo
                mequipo, //id equipo
                mprestamo //id préstamo
            },
        
            // especifica si será una petición POST o GET
            type : 'POST',
        
            // el tipo de información que se espera de respuesta
            dataType : 'json',
        
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success : function(json) {
                /*$('<h1/>').text(json.title).appendTo('body');
                $('<div class="content"/>')
                    .html(json.html).appendTo('body');*/
                let mensaje = document.getElementById("mmensaje"); 
                if(json.error ==0){
                    mensaje.className = "alert alert-success"
                    mensaje.innerHTML = `${json.mensaje}`
                    
                }else{
                    mensaje.className = "alert alert-danger"
                    mensaje.innerHTML = `${json.mensaje}`
                    
                }
                console.log(json)
            } 
        });
    });

    //ver datos de petición de préstamo
    $("#botones .detalle").click(function (e) { 
        e.preventDefault();
        let id = $(this).val()  
        $.ajax({
            // la URL para la petición
            url : '../../controllers/verDatosPrestamo.php', 
            // la información a enviar
            // (también es posible utilizar una cadena de datos)
            data : {id:id},
        
            // especifica si será una petición POST o GET
            type : 'POST',
        
            // el tipo de información que se espera de respuesta
            dataType : 'json',
        
            // código a ejecutar si la petición es satisfactoria;
            // la respuesta es pasada como argumento a la función
            success : function(json) {
                /*   
                "especificaciones" => $r[0]["TEQ_DETALLE"],
                "detalle" => $r[0]["PRE_DETALLE"] 
                */  
                const formVer =  document.getElementById("modalVerDatos");
                const myInput = `
                                <div>
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Rut del prestario</label>
                                <input value="${json.rut}" name="vrut" type="text" class="form-control" id="vrut" placeholder="Rut" readonly>
                                </div> 
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Nombre del prestario</label>
                                <input value="${json.nombres} ${json.paterno} ${(json.materno)?json.materno:''}" name="vnombre" type="text" class="form-control" id="vnombre" placeholder="Nombre" readonly>
                                </div> 
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Departamento</label>
                                <input value="${json.departamento}" name="vdepartamento" type="text" class="form-control" id="vdepartamento" placeholder="Departamento" readonly>
                                </div> 
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Cargo del prestario</label>
                                <input value="${json.cargo}" name="vcargo" type="text" class="form-control" id="vcargo" placeholder="Cargo" readonly>
                                </div> 
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Modelo del equipo</label>
                                <input value="${json.codigo}" name="vcodigo" type="text" class="form-control" id="vcodigo" placeholder="Codigo" readonly>
                                </div>
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Especificaciones del equipo</label>
                                <input value="${json.especificaciones}" name="vespecificacion" type="text" class="form-control" id="vespecificacion" placeholder="Especificación" readonly>
                                </div> 
                                <div class="mb-3 form-group"> 
                                <label for="recipient-name" class="col-form-label">Detalle del préstamo</label>
                                <textarea  value="${(json.detalle)?json.detalle:''}" name="vdetalle" type="text" class="form-control" id="vdetalle" placeholder="Detalle" rows="10" cols="50" readonly>${(json.detalle)?json.detalle:''}</textarea>
                                </div> 
                                </div>
                             `
                formVer.innerHTML = myInput; console.log(json.detalle);
            } 
        });
    });
    //cancelar petición de préstamo  
    $("#btnCancelar").click(function (e) { 
        e.preventDefault();
        
    }); 
});