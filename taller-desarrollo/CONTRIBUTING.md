## Guía de Contribución

## Equipo de Desarrollo

Autores


Omaro Aburto Leiva - Líder de proyecto - oaburto@alumnos.ubiobio.cl

Eduardo Venegas Jiménez - Analista - edvenega@alumnos.ubiobio.cl

Ignacio Aqueveque Carvajal  - Ingeniero de BD - ignacio.aqueveque1701@alumnos.ubiobio.cl 

Nicolás Condeza Rifo  - Desarrollador - nicolas.condeza1401@alumnos.ubiobio.cl

Gerardo Alejandro Maturana Rojas  - Desarrollador - gerardo.maturana1501@alumnos.ubiobio.cl

Yerko Hurtado Arce - Desarrollador - yerko.hurtado1901@alumnos.ubiobio.cl




## Interacción con el repositorio

- Omaro Aburto Leiva 
   Contribuciones: <br/>Issue #5 Login<br/> 
                   mediante commit subió todos los modelos<br/>
                   validaciones de todo los modelos<br/>
                   

- Eduardo Venegas Jiménez
   Contribuciones: <br/>Issue #1 Validar prestamo<br/>
                   Issue #9 Tabla controller<br/>
                   Issue #11 Prestamos dos<br/>
                   Issue #14 Modulo admin completado 2<br/>
                   Issue #15 Administrador js<br/>
                   Issue #16 Modificar prestamo admin<br/>

- Ignacio Aqueveque Carvajal
   Contribuciones: <br/>Issue #6 Actualizacion-de-readme<br/>
                   Creación de tablas<br/>
                   Creación de vistas<br/>
                   Creacion de insersiones con plataforma mookaro.com<br/>
                   Creacion de guia de docker<br/>

- Nicolás Condeza Rifo
  Contribuciones: <br/>Issue #2 Validar Fecha Inicio<br/>
                  Issue #7 Datatable<br/>
                  Issue #8 Crear modelo datatable<br/>
                  Issue #10 Prestamo<br/>
                  Issue #13 Modulo admin completado<br/>
                  Issue #17 Crear administrador php<br/>
                  Issue #18 Controlador Modificar Prestamo Aceptado<br/>

- Gerardo Alejandro Maturana Rojas
   Contribuciones: <br/>creación de la plantilla de intranet, login y error<br/>
                   Creo método para la vista de la clase préstamo, usuario y equipo<br/>
                   __presento problemas con la cuenta de git y fue informado al servicio de informatica de la universidad__<br/>



- Yerko Hurtado Arce
   Contribuciones: <br/>Issue #3 Validar Fecha Fin<br/>
                   Issue #4 Agregar menu<br/>
                   Issue #12 Prestamo tres<br/>

## **informacion adicional** 
- Se consulto al jefe de grupo para subir archivos a la rama master con merge requests.
- No todo lo trabajado se realizo con issue dado que esta informacion nos llego cuando ya estabamos trabajando, esto se hablo con el profesor Marcelo Pinto en una reunion por zoom. por eso no refleja completamente el trabajo de todos, dejar claro que todos pusieron de su parte.
- No todo lo subido se hizo con merge requests, dado que anteriormente se subieron algunos datos directamente con master, y al finalizar el proyecto se obto de igual manera por esto dado que era revizado por todo el grupo.


## Editor Config
Busca e instala Markdown Preview Enhanced, como plugin para tu Editor de Código, y de esta forma, automáticamente se configurarán las opciones para editores de código usadas en este proyecto.


## Arquitectura del Sistema - conexcion bd

Para conectar a la base de datos debes tener el entorno docker(docker-compose.yml, Dockerfile y - rest.conf) luego:
- Ejecutar los script de creación de base de tablas, inserción y creación de vistas que están en la carpeta SQL en gitlab.

- Ejecutar el script del archivo `alter.sql`, el cual contiene el script para agregar el atributo USU_RUT y actualiza los datos de una secretaria y de un administrador.

    - datos de secretaria usuario: klebretond@spotify.com, contraseña: Abc123def
    - datos de administrador usuario: jyukhtinq@cpanel.net, contraseña: Abc123def 
  
-  Actualizar los datos de conexcion de base de datos del archivo db.php ubicado en `/models/db.php`.






