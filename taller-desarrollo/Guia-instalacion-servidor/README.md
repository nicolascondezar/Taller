# Pasos para la configuración del servidor 

1. En caso de tener alguna distribución de Linux en su equipo personal haga una conexión por SSH desde la consola.

    En el caso de Windows, descargar el software `PuTTY` e instalar. Tendrá una interfaz como la siguiente y debe agregar el `IP` 146.83.198.35 y el puerto `SSH` 1058.


    - __PuTTY__

    ![PuTTY](./putty.png) 

2. Después de que se hayan agregado los datos ya mencionados aparecerá la siguiente consola, donde se tendrá que ingresar con su `usuario` usuario6 y `contraseña` U$er6. 

    
    - __Consola__

    ![Consola](./consola.png)


3. Una vez listo se visualizará una consola como la siguiente.


     - __Consola__

    ![Consola](./se_vera.png)

4. Luego de haber ingresado se tendrá que instalar `Apache`.

    
    - __Primer paso__

    Cambie el usuario a `root` con el comando `su -` e ingresando la clave `O4burto.2021$` y ejecute lo siguiente.

    `apt update`
    `apt install apache2 -y`

5. Ya finalizada la instalación de `Apache` se debe instalar `PHP`.

   
    - __Introduzca uno a uno los siguientes comandos__

    `apt update`
    `apt upgrade`
    `apt install php7.3 -y`
    
    - Nota: Para listar los módulos que se requiere instalar de PHP debe usar `apt search php7.3`.

     - __Ahora se instalaran los módulos necesarios__

    `apt install php7.3-common php7.3-mysql php7.3-xml php7.3-curl php7.3-gd php7.3-cli php7.3-dev php7.3-imap php7.3-mbstring php7.3-opcache php7.3-intl -y`
    - Reiniciar el apache con el comando `service apache2 restart`

    - Nota: Para listar los módulos PHP instalados se debe usar el comando `php -m`.

6. Posteriormente se procederá a instalar `GIT` para clonar el proyecto.


    - __Introduzca los siguientes comandos__

    `apt update`
    `apt install git -y`

7. en caso de no tener editor de texto, se debe instalar el editor de texto `nano` con los siguientes comandos:

    `apt update`
    `apt install nano`

8. Ingresar al directorio con el comando `cd /etc/apache2/sites-available`, luego editar el archivo `000-default.conf` con el comando `nano 000-default.conf`. Borre el contenido del archivo y copie el contenido del archivo `rest.conf` que está en la siguiente ruta en git `/guia-instalacion-docker/archivo-entorno-docker/`

9. Para finalizar se debe clonar el proyecto desde `GitLab`.


    - __Pasos a seguir__

    - Diríjase a la dirección var/www/html con el comando `cd /var/www/html`.
    - Dentro de la carpeta clonar el proyecto por HTTP.
    - Ingresar a la carpeta `taller-desarrollo` con el comando `cd taller-desarrollo` mover los archivos de a la carpeta `html` con el comando `mv .git controllers helpers middlewares sockets README.md asset index.php models views /var/www/html`.

    -Nota: Si se quiere eliminar la carpeta `taller-desarrollo` se debe usar el comando `rm -rfv taller-desarrollo`.

    - Para visualizar el proyecto poner en el navegador la URL `IP:Puerto_Apache`.