<?php   
    require_once("./helpers/rutas.php");
    require_once("./helpers/template.php");
    require_once("./controllers/login.php");
    require_once("./helpers/menuConf.php");
    require_once("./controllers/secretaria.php");
    require_once("./controllers/administrador.php");
    $rutas = new Rutas(); 
    if($rutas->validarRutas()===1 && substr_count($_SERVER["REQUEST_URI"],"/")<3){   
        switch ($rutas->getUrl()) {
            //vista del logingi
            case 'login':
                $myTemplate = new Template("./views/login.html", [
                    "title" => "1 Acceso - Universidad del Sur ",
                    "error" => ($rutas->getUrl2()=="error")
                                ?"<div class='card-footer'>
                                    <div class='alert alert-danger' role='alert'>
                                        Error en el email o el password
                                    </div>
                                </div>":" "   
                ]); 
                echo $myTemplate;
                break;
            //vista de intranet
            case 'intranet': 
                session_start();
                if(isset($_SESSION["usuario"])){
                    switch ($_SESSION["usuario"]["tipo"]) {
                        case 1:
                    $myTemplate = new Template("./views/menu.html", [
                        "title" => "intranet - Universidad del Sur",  
                        "menu" => menuConfi($_SESSION["usuario"]["tipo"]),
                        "nombre" => $_SESSION["usuario"]["nombre"],
                        "tabla" => dataTable($_SESSION["usuario"]["id"]),
                        "modal" => modalesSecretaria($_SESSION["usuario"]["id"]),
                        "script" => "<script src='./asset/js/secretaria.js'></script>"
                    ]); 
                    echo $myTemplate;
                    break;
                    case 3:
                        $myTemplate = new Template("./views/menu.html", [
                            "title" => "intranet - Universidad del Sur",  
                            "menu" => menuConfi($_SESSION["usuario"]["tipo"]),
                            "nombre" => $_SESSION["usuario"]["nombre"],
                            "tabla" => dataTableAdm($_SESSION["usuario"]["id"]),
                            "modal" => modalesAdm($_SESSION["usuario"]["id"]),
                            "script" => "<script src='./asset/js/administrador.js'></script>"
                        
                        ]); 
                        echo $myTemplate;
                        break;
                        default:
                        break;
                    }
                }else{
                    header("Location: ./login/error");
                }
                break; 
            //se valida la autentificación
            case 'autentificar': 
                $login = new Login($_POST["email"], $_POST["password"]);
                if($login->validarLogin()==1){ 
                    session_start();
                    $_SESSION["usuario"] = $login->datosUsuario();
                    header("Location: ./intranet");
                }else{
                    header("Location: ./login/error");
                }
                break;  
            case 'salir': 
                session_start();  
                session_destroy();
                header("Location: ./login/");
                break;
            //directorio raiz
            default:
                session_start();
                if(isset($_SESSION["usuario"])){
                    header("Location: ./intranet");
                }
                $myTemplate = new Template("./views/login.html", [
                    "title" => "Acceso - Universidad del Sur",
                    "error" => ""  
                ]); 
                echo $myTemplate;
                break;
        }
    }else{
        //si existe una url inválida
        if(substr_count($_SERVER["REQUEST_URI"],"/")>2) header("Location: http://localhost:8080/error");
        $myTemplate = new Template("./views/error.html", [ ]); 
        echo $myTemplate; 
    }

