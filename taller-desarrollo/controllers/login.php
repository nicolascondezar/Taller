<?php
require_once("./helpers/validarUsuario.php");
require_once("./models/usuario.php");
require_once("./models/db.php");
class Login{
    private $email;
    private $password;
    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
    public function validarLogin(){

        $validar = new validarUsuario();
        $db = new DB();
        $usuario = new Usuario($db->conectar());
        $datosUsuario = $usuario->listarUsuarioEmail($this->email); 
        return (!is_null($this->email)
                && !is_null($this->password)
                && $validar::validarFormatoPassword($this->password)==1
                && $validar::validarEmail($this->email)
                && !is_null($datosUsuario)
                && $validar::validarHashPassword($this->password, $datosUsuario[0]["USU_PASSWORD"])
                )?1:0;
    }
    public function datosUsuario(){ 
        $db = new DB();
        $usuario = new Usuario($db->conectar());
        $datosUsuario = $usuario->listarUsuarioEmail($this->email); 
        $datos = [
            "nombre" => $datosUsuario[0]["USU_NOMBRES"]." ".$datosUsuario[0]["USU_APELLIDO_PATERNO"],
            "tipo" => $datosUsuario[0]["TUS_ID"], 
            "id" => $datosUsuario[0]["USU_ID"]
        ];
        return $datos;

    }
}