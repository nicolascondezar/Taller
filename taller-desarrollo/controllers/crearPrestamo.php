<?php
header('Content-Type: application/json;charset=utf-8');
require_once("../helpers/validarUsuario.php");
require_once("../helpers/validarPrestamo.php");
require_once("../helpers/validarEquipo.php");
require_once("../models/prestamo.php");
require_once("../models/usuario.php");
require_once("../models/db.php");

$rut        = filter_var($_POST["crut"], FILTER_SANITIZE_STRING);
$equipo     = filter_var($_POST["cid_equipo"], FILTER_SANITIZE_NUMBER_INT);
$secretaria = filter_var($_POST["csecretaria"], FILTER_SANITIZE_NUMBER_INT); 

$mensaje= ""; 
if(validarUsuario::validarRut($rut)==1){
    $cpdo= new DB(); 
    $conectar = $cpdo->conectar();
    $usuario =  new Usuario($conectar); 
    //consulta si existe el usuario en la base de datos
    $usu = $usuario->listarUsuarioRut($rut); 
    if($usu){
 
        $prestamo = new Prestamo($conectar);
        $resultado = $prestamo->crearPrestamo($usu[0]["USU_ID"], $equipo, $secretaria);
        if($resultado ==1){
            $mensaje =  "Solicitud de préstamo creado";
            $error=0;
        }else{
            $mensaje = "Error de la base de datos";
            $error=1;
        } 
    }else{
        $mensaje = "No existe el usuario";
        $error=1;
    }
}else{
    $mensaje= "Ingrese un rut válido";
    $error=1;
}
 
echo json_encode([
    "mensaje" => $mensaje, 
    "equipo" => $equipo,
    "error" => $error,
    "secretaria" => $secretaria
]);
