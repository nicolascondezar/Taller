<?php
require_once("./models/equipo.php"); 
require_once("./models/prestamo.php");
require_once("./models/db.php");
function dataTableAdm($id){
    $cpdo= new DB(); 
    $prestamo = new Prestamo($cpdo->conectar());
    $prestadoActivo = $prestamo->listarPrestamosActivosAdm($id);
    $aux = "";
    $i=0;
    foreach ($prestadoActivo as $value) {
        $newDate = date("d/m/Y", strtotime($value["PRE_FECHA_INCIO"]));
        $newDate2 = date("d/m/Y", strtotime($value["PRE_FECHA_FINAL"]));
        switch ($value["PRE_ESTADO"]) {
            case 'a':
                $estado = "Activo";
                break; 
            case 'r':
                $estado = "Rechazado";
                break;
            case 'f':
                $estado = "Finalizado";
                break;
            case 'p':
                $estado = "Pendiente";
                break;
        }
        $aux .= "<tr>
                    <th>".$value["PRE_ID"]."</th> 
                    <th>".$value["EQU_ID"]." - ".$value["TEQ_CODIGO"]."</th>
                    <th>".$value["USU_RUT"]."</th>
                    <th>".$value["USU_NOMBRES"]." ".$value["USU_APELLIDO_PATERNO"]."</th> 
                    <th>".$newDate."</th>
                    <th>".$newDate2."</th>
                    <th>".$estado."</th>  
                    <th><button value='".$value["PRE_ID"]."'id='btnAceptar".$i."' class='btn btn-success aceptar'> <i class='fa fa-check'></i> </button>
                        
                         <button class='btn btn-danger modificar' value='".$value["PRE_ID"]."' id='btnRechazar".$i."' data-toggle='modal' data-target='#ModalRechazado'><i class='fa fa-times'></i></i></button></a></th>
        
                </tr> "; 
                $i++;
    }

    $tabla= "<table id='example' class='table table-striped table-bordered' style='width:100%; box-sizing: 1p 1px 1px black;'>
                <thead style='color: white; background-color: #3C8DBC;'>
                    <tr>
                        <th>ID</th> 
                        <th>ID EQUIPO</th>
                        <th>RUT</th>
                        <th>NOMBRE USUARIO</th> 
                        <th>FECHA INICIO</th>
                        <th>FECHA FINAL</th>  
                        <th>ESTADO</th>
                        <th>ACCION</th>
                    </tr>
                </thead>
                <tbody id='botones2'>
                    ".$aux."
                </tbody> 
            </table>";
    
    return $tabla;
}
function modalesAdm($id){
    $equipo = new Equipo();
    $datosEquipos = $equipo->listarEquiposActivosadm();
    $aux= "";
    foreach ($datosEquipos as $value) {
       if($value['EQU_ID']!="")
       $aux .= '<option value="'.$value['EQU_ID'].'">'.$value['EQU_ID'].' - '.$value['TEQ_CODIGO'].' - '.$value['TEQ_DETALLE'].'</option>';
    }
    $modales = '<div class="modal fade " id="ModalRechazado" role="dialog" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" >
    <div class="modal-content">
        <div class="modal-header" style="border-bottom: 2px solid rgb(39, 54, 77); background-image: linear-gradient( 109.6deg,  rgba(45,116,213,1) 11.2%, rgba(121,137,212,1) 91.2% ); color: white; text-shadow: 1px 1px rgb(12, 70, 146);" > 
        <h5 class="modal-title" id="exampleModalLabel"> Préstamo</h5> 
        </div>
        <div class="modal-body">
        <form id="crearprestamo" style="padding: 20px;">
            <div class="mb-3 form-group">
            <label for="recipient-name" class="col-form-label">Detalle del rechazo</label>
            
            </div> 
            <span><textarea name="respuesta" id="respuesta"  required maxlength="500"  style="width:500px; height:100px;"> </textarea></span>
             <input type="hidden" id="id_prestamo">
             <div id="mmensaje"></div>
        </form>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> Cerrar</button>
        <button id="btnModificar" type="button" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Registrar</button>
        </div>
    </div>
    </div>
</div>
                <!-- Fin modal Ver Datos -->
                ';
    return $modales;
}