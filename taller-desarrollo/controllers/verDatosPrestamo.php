<?php
header('Content-Type: application/json;charset=utf-8'); 
require_once("../models/prestamo.php"); 
require_once("../models/db.php"); 

$idprestamo = filter_var($_POST["id"], FILTER_SANITIZE_NUMBER_INT); //id préstamo
$cpdo= new DB(); 
$conectar = $cpdo->conectar();
$prestamo = new Prestamo($conectar);
$r= $prestamo->verPrestamosId($idprestamo);

echo json_encode([ 
    "nombres" => $r[0]["USU_NOMBRES"],  
    "paterno" => $r[0]["USU_APELLIDO_PATERNO"],
    "materno" => $r[0]["USU_APELLIDO_MATERNO"],
    "rut" => $r[0]["USU_RUT"],
    "departamento" => $r[0]["USU_DEPARTAMENTO"],
    "cargo" => $r[0]["USU_CARGO"],
    "codigo" => $r[0]["TEQ_CODIGO"],
    "especificaciones" => $r[0]["TEQ_DETALLE"],
    "detalle" => $r[0]["PRE_DETALLE"]    
]);