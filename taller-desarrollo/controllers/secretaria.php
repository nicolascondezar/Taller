<?php
require_once("./models/equipo.php"); 
require_once("./models/prestamo.php");
require_once("./models/db.php");
function dataTable($id){
    $cpdo= new DB(); 
    $prestamo = new Prestamo($cpdo->conectar());
    $prestadoActivo = $prestamo->listarPrestamosActivos($id);
    $aux = "";
    $i=0;
    foreach ($prestadoActivo as $value) {
        $newDate = date("d/m/Y", strtotime($value["PRE_FECHA_INCIO"]));
        switch ($value["PRE_ESTADO"]) {
            case 'a':
                $estado = "Activo";
                break; 
            case 'r':
                $estado = "Rechazado";
                break;
            case 'f':
                $estado = "Finalizado";
                break;
            case 'p':
                $estado = "Pendiente";
                break;
        }
        $aux .= "<tr>
                    <th>".$value["PRE_ID"]."</th> 
                    <th>".$value["EQU_ID"]." - ".$value["TEQ_CODIGO"]."</th>
                    <th>".$value["USU_RUT"]."</th>
                    <th>".$value["USU_NOMBRES"]." ".$value["USU_APELLIDO_PATERNO"]."</th> 
                    <th>".$newDate."</th>
                    <th>".$estado."</th>  
                    <th><button id='btnModificar".$i."' class='btn btn-warning modificar' value='".$value["PRE_ID"]."/".$value["USU_RUT"]."/".$value["EQU_ID"]."/".$value["TEQ_CODIGO"]."'
                        data-toggle='modal' 
                        data-target='#myModalModificar'>
                        <i class='fa fa-cog' aria-hidden='true'></i>
                        Modificar
                        </button>
                    </th>
                   <!-- <th><button id='btnDetalle".$i."' class='btn btn-primary detalle' value='".$value["PRE_ID"]."'
                        data-toggle='modal' 
                        data-target='#myModalVer'>
                        <i class='fa fa-eye' aria-hidden='true'></i>
                        Detalle
                        </button>
                    </th>-->
                </tr> "; 
                $i++;
    }

    $tabla= "<table id='example' class='table table-striped table-bordered' style='width:100%; box-sizing: 1p 1px 1px black;'>
                <thead style='color: white; background-color: #3C8DBC;'>
                    <tr>
                        <th>ID</th> 
                        <th>ID EQUIPO</th>
                        <th>RUT</th>
                        <th>NOMBRE USUARIO</th> 
                        <th>FECHA DE PETICIÓN</th>
                        <th>ESTADO</th>  
                        <th>MODIFICAR</th>
                        <!--<th>DETALLE</th>-->
                    </tr>
                </thead>
                <tbody id='botones'>
                    ".$aux."
                </tbody> 
            </table>";
    
    return $tabla;
}
function modalesSecretaria($id){
    $equipo = new Equipo();
    $datosEquipos = $equipo->listarEquiposActivos();
    $aux= "";
    foreach ($datosEquipos as $value) {
       if($value['EQU_ID']!="")
       $aux .= '<option value="'.$value['EQU_ID'].'">'.$value['EQU_ID'].' - '.$value['TEQ_CODIGO'].' - '.$value['TEQ_DETALLE'].'</option>';
    }
    $modales = '<button class="btn btn-success" 
                    data-toggle="modal" 
                    data-target="#myModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Crear Préstamo</button>
                <br/>

                <div class="modal fade " id="myModal" role="dialog" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" >
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: 2px solid rgb(39, 54, 77); background-image: linear-gradient( 109.6deg,  rgba(45,116,213,1) 11.2%, rgba(121,137,212,1) 91.2% ); color: white; text-shadow: 1px 1px rgb(12, 70, 146);" > 
                        <h5 class="modal-title" id="exampleModalLabel">Crear Préstamo</h5> 
                        </div>
                        <div class="modal-body">
                        <form id="crearPrestamo" style="padding: 20px;">
                            <div class="mb-3 form-group">
                            <label for="recipient-name" class="col-form-label">Usuario</label>
                            <input name="crut" type="text" class="form-control" id="crut" placeholder="Rut" required>
                            </div> 
                            <div class="mb-3 form-group">
                                <label for="recipient-name" class="col-form-label">Equipo</label><br/>
                                <select id="cequipo" name="cid_equipo" class="form-control form-group-lg">'.$aux.'
                                </select>
                            </div> 
                            <input id="csecretaria" name="csecretaria" type="hidden" value="'.$id.'">
                            
                        </form>
                        <div id="cmensaje"></div>
                        </div>
                        <div class="modal-footer">
                        
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> Cerrar</button>
                        <button id="btnCrear" type="button" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Registrar</button>
                        </div>
                    </div>
                    </div>
                </div>
                
                <!-- Modal Modificar -->
                <div class="modal fade " id="myModalModificar" role="dialog" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" >
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: 2px solid rgb(39, 54, 77); background-image: linear-gradient( 109.6deg,  rgba(45,116,213,1) 11.2%, rgba(121,137,212,1) 91.2% ); color: white; text-shadow: 1px 1px rgb(12, 70, 146);" > 
                        <h5 class="modal-title" id="exampleModalLabel">Modificar Préstamo</h5> 
                        </div>
                        <div class="modal-body">
                        <form id="modificarPrestamo" style="padding: 20px;">
                            <div class="mb-3 form-group">
                            <label for="recipient-name" class="col-form-label">Usuario</label>
                            <input name="mrut" type="text" class="form-control" id="mrut" placeholder="Rut" required>
                            </div> 
                            <div class="mb-3 form-group">
                                <label for="recipient-name" class="col-form-label">Equipo</label><br/>
                                <select id="mequipo" name="mid_equipo" class="form-control form-group-lg">'.$aux.'
                                </select>
                            </div> 
                            <input id="msecretaria" name="msecretaria" type="hidden" value="'.$id.'">
                            <input id="mprestamo" name="mprestamo" type="hidden">
                        </form>
                        <div id="mmensaje"></div>
                        </div>
                        <div class="modal-footer">
                        
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> Cerrar</button>
                        <button id="btnModificar" type="button" class="btn btn-success"><i class="fa fa-plus" aria-hidden="true"></i> Registrar</button>
                        </div>
                    </div>
                    </div>
                </div>    
                <!-- Fin modal modificar -->

                <!-- Modal Ver Datos -->
                <div class="modal fade " id="myModalVer" role="dialog" tabindex="-1"  aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" >
                    <div class="modal-content">
                        <div class="modal-header" style="border-bottom: 2px solid rgb(39, 54, 77); background-image: linear-gradient( 109.6deg,  rgba(45,116,213,1) 11.2%, rgba(121,137,212,1) 91.2% ); color: white; text-shadow: 1px 1px rgb(12, 70, 146);" > 
                        <h5 class="modal-title" id="exampleModalLabel">Datos de Préstamo</h5> 
                        </div>
                        <div class="modal-body">
                        <form id="modalVerDatos" style="padding: 20px;">
                            
                        </form> 
                        <div class="modal-footer">
                        
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-stop-circle-o" aria-hidden="true"></i> Cerrar</button>
                         
                        </div>
                    </div>
                    </div>
                </div>    
                <!-- Fin modal Ver Datos -->
                ';
    return $modales;
}