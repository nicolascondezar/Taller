<?php
/*********************************************************************** 
 DETALLE: ES EL CONTROLADOR QUE TIENE LA TABLA A UTILIZAR
************************************************************************* */
include '../models/tabla.php';

$tabla = new Tabla();
if($_POST['funcion']=="listar"){
    $tabla->mostrar();
    $json = array();
    foreach ($tabla->tablas as $data) {
       $json['data'][]=$data;
    }
    $jsonstring = json_encode($json);
    echo $jsonstring;
}
