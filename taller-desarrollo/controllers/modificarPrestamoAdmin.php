<?php
header('Content-Type: application/json;charset=utf-8');

require_once("../helpers/validarPrestamo.php");
require_once("../models/prestamo.php");
require_once("../models/db.php");

$respuesta        = filter_var($_POST["respuesta"], FILTER_SANITIZE_STRING);//rut
$idEquipo     = filter_var($_POST["idEquipo"], FILTER_SANITIZE_NUMBER_INT);//id equipo

$mensaje= ""; //mensaje de respuesta del seridor
if(validarPrestamo::validarDetallePrestamo($respuesta) == 1 && $respuesta != "" ){
    $cpdo= new DB(); 
    $conectar = $cpdo->conectar();
     
    $prestamo = new Prestamo($conectar);
    $resultado = $prestamo->actualizarPrestamoAdmin($idEquipo, $respuesta);
        if($resultado ==1){
            $mensaje =  "Se ha rechazado la petición de préstamo";
            $error=0;
        }else{
            $mensaje = "Error de la base de datos";
            $error=1;
        }    

}else {
            $mensaje = "Error en el detalle del rechazo";
            $error=1;
}

echo json_encode([
    "mensaje" => $mensaje,   
    "error" => $error,
    
]);
