<?php
header('Content-Type: application/json;charset=utf-8');

require_once("../helpers/validarPrestamo.php");
require_once("../models/prestamo.php");
require_once("../models/db.php");

$idPrestamo     = filter_var($_POST["idPrestamo"], FILTER_SANITIZE_NUMBER_INT);//id equipo

$mensaje= ""; //mensaje de respuesta del seridor
$cpdo= new DB(); 
$conectar = $cpdo->conectar(); 
$prestamo = new Prestamo($conectar);
$resultado = $prestamo->actualizarPrestamoAceptadoAdmin($idPrestamo);
    if($resultado ==1){
        $mensaje =  "Se ha aceptado la petición de préstamo";
        $error=0;
    }else{
        $mensaje = "Error de la base de datos";
        $error=1;
    }    

echo json_encode([
    "mensaje" => $mensaje,   
    "error" => $error,
    
]);
